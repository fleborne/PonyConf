Add a new conference
====================

We suppose our conference is hosted at ``cfp.example.org``::

  $ ./manage.py addsite cfp.example.org

The slides for this conference will be uploaded in
``/media/example/``, in a ``cfp.example.org`` sub-directory.

The displayed name of the conference is stored in the ``Conference``
model and can be modified from the web interface.

Add the domain in ``ponyconf/local_settings.py``::

  ALLOWED_HOSTS = [ 'cfp.example.org' ]

Make sure you web server is configured to serve this new domain.

Configure the e-mails.

Go to ``https://cfp.example.org``, log-in and configure your conference.
