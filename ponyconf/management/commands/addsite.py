from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Add a site'

    def add_arguments(self, parser):
        parser.add_argument('domain', type=str)

    def handle(self, *args, **options):
        site, created = Site.objects.get_or_create(domain=options['domain'], name=options['domain'])
        if created:
            self.stdout.write(self.style.SUCCESS('Created site {}'.format(site)))
        else:
            self.stdout.write(self.style.NOTICE('Site {} already exists'.format(site)))
